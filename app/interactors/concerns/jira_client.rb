module JiraClient
  extend ActiveSupport::Concern

  included do
    def jira_client
      @jira_client ||= JIRA::Client.new({username: ENV['JIRA_USERNAME'],
                                         password: ENV['JIRA_PASSWORD'],
                                         site: ENV['JIRA_SITE'],
                                         context_path: ENV['JIRA_CONTEXT_PATH'],
                                         read_timeout: 120,
                                         use_ssl: ENV['JIRA_USE_SSL'] == '1',
                                         auth_type: :basic})
    end
  end
end
