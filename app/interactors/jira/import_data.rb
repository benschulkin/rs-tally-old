module Jira
  class ImportData
    include Interactor
    include JiraClient

    def call
      jira_client.Status.all.each {|status| SyncStatus.call(status: status)}
      jira_client.Project.all.each {|project| SyncProject.call(project: project)}
      jira_client.Issue.all.each {|issue| SyncIssue.call(issue: issue)}
    end
  end
end
