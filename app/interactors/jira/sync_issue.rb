module Jira
  class SyncIssue
    include Interactor

    def call
      ticket = Ticket.find_or_initialize_by jira_id: context.issue.id
      status = Status.find_by jira_id: context.issue.status.id

      ticket.jira_key = context.issue.key
      ticket.summary = context.issue.summary
      ticket.low_quote_time = 0 # todo what does it mean?
      ticket.high_quote_time = 0 # todo what does it mean?
      ticket.actual_time = context.issue.timespent
      ticket.description = context.issue.description
      ticket.status_id = status.id
      ticket.save
    end
  end
end
