module Jira
  class SyncProject
    include Interactor

    def call
      project = Project.find_or_initialize_by jira_id: context.project.id

      project.jira_key = context.project.key
      project.name = context.project.name
      project.save
    end
  end
end
