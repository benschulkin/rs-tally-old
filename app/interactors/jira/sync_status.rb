module Jira
  class SyncStatus
    include Interactor

    def call
      status = Status.find_or_initialize_by jira_id: context.status.id

      status.name = context.status.name
      status.description = context.status.description
      status.save
    end
  end
end
