class Client < ApplicationRecord
  has_one :extra, class_name: ClientExtra, autosave: true

  accepts_nested_attributes_for :extra

  validates :name, presence: true

end
