class Project < ApplicationRecord
  belongs_to :client
  has_one :extra, class_name: ProjectExtra, autosave: true

  accepts_nested_attributes_for :extra

end
