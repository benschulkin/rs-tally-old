ActiveAdmin.register Project do
  actions :all, except: [:new, :destroy]

  permit_params :client_id, extra_attributes: [:quickbooks_client_id, :bill_rate]

  form do |f|
    semantic_errors

    inputs do
      object.build_extra unless object.extra
      input :client
      input :name, input_html: {readonly: true}
      input :jira_key, input_html: {readonly: true}
      has_many :extra, allow_destroy: false, new_record: false do |a|
        a.input :quickbooks_client_id
        a.input :bill_rate
      end
    end

    actions
  end

end
