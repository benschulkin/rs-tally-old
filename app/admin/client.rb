ActiveAdmin.register Client do
  permit_params :name, extra_attributes: [:quickbooks_client_id, :bill_rate]

  form do |f|
    semantic_errors

    inputs do
      object.build_extra unless object.extra
      input :name
      has_many :extra, allow_destroy: false, new_record: false do |a|
        a.input :quickbooks_client_id
        a.input :bill_rate
      end
    end

    actions
  end
end
