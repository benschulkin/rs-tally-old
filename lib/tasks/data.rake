namespace :data do
  desc 'Import data from JIRA into app'
  task import: :environment do
    Jira::ImportData.call
  end

end
