class CreateTickets < ActiveRecord::Migration[5.0]
  def change
    create_table :tickets do |t|
      t.integer :jira_id
      t.string :jira_key
      t.string :summary
      t.integer :low_quote_time # todo what does it mean?
      t.integer :high_quote_time # todo what does it mean?
      t.integer :actual_time # seconds
      t.string :description
      t.belongs_to :status

      t.timestamps
    end
  end
end
