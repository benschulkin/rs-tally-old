class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name, null: false
      t.string :jira_key
      t.integer :jira_id
      t.belongs_to :client, foreign_key: true

      t.timestamps
    end
  end
end
