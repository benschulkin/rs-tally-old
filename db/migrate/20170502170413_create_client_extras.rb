class CreateClientExtras < ActiveRecord::Migration[5.0]
  def change
    create_table :client_extras do |t|
      t.belongs_to :client, foreign_key: true
      t.string :quickbooks_client_id
      t.float :bill_rate
    end
  end
end
