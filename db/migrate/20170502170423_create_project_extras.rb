class CreateProjectExtras < ActiveRecord::Migration[5.0]
  def change
    create_table :project_extras do |t|
      t.belongs_to :project, foreign_key: true
      t.string :quickbooks_client_id
      t.float :bill_rate
    end
  end
end
