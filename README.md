# Red Sequoia

## Development installation

Run docker containers
```
cd docker
docker-compose up -d
```


Install dependencies
```
docker-compose exec bia_ruby zsh
bundle install
rake db:migrate
rake db:seed
```

Run JIRA
```
docker-compose exec bia_jira atlas-run-standalone --product jira
```
